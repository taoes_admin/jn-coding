#!/bin/bash
RED='\033[37;31m'      # 红
GREEN='\033[37;32m'    # 绿
YELOW='E[1;33m'    # 黄
BLUE='E[1;34m'     # 蓝
PINK='E[1;35m'     # 粉红
RES='\033[0m'         # 清除颜色

 echo -e "${RED} 开始启动构建 工作，请稍后.... ${RES}"
 sleep 3


function clean_cache(){
    rm -rf public
    print_msg $? "缓存文件清理失败，文件可能不存在" "缓存文件清理完成"
}

function print_msg(){
    if [[ $1 -ne 0 ]]
    then
        echo -e "${RED}  $2 ${RES}"
        exit -1
    else
        echo -e "${GREEN}  $3 ${RES}"
    fi
}





clean_cache
echo -e "${GREEN}  清理文件完成，开始安装依赖等信息 ${RES}"
yarn install
print_msg $? "yarn 安装依赖库失败，请检查配置" "Yarn 安装依赖完成,正在准备编译构建"


hexo clean && hexo g
print_msg $? "yarn 构建失败，请检查配置" "Yarn 构建完成,正在准备上传到服务器"


scp -r ./public ubuntu@www.zhoutao123.com:/home/ubuntu/wiki/
print_msg $? "上传文件失败，请检查网络等配置" "上传文件完成，开始清理编译文件"

clean_cache
